# SafeBoda case study

## Considerations

- Implemented using MVVM  & LiveData with Repository Pattern for data access
- Network access implemented with Retrofit
- Persistence implemented with Room
- Due to time restrictions, some problems have not been solved
	- JSON format: Lufthansa responses where arbitrarily returning objects or arrays (airport names, flight connections), so automatic parsing failed
		- Bypassed configuring calls to hardcoded language (EN) and only requesting direct flights
		- This can be solved writing a custom type adapter to wrap these objects as lists
- API access keys are configured out of the repository in the *gradle.properties* file as:
	`lhAppKey="key_here"`  
	`lhAppSecret="secret_here"`
