package com.danielcruz.sbcasestudy.network

import com.danielcruz.sbcasestudy.BuildConfig
import com.danielcruz.sbcasestudy.data.AirportResponse
import com.danielcruz.sbcasestudy.data.ScheduleResponse
import com.danielcruz.sbcasestudy.data.TokenResponse
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.io.IOException

interface LufthansaServices {


    // limited to ENG because names where returned inside and array or as an object depending on the airport
    @GET("/v1/references/airports?limit=100&lang=en")
    fun getAirports(@Query("offset") offset: Int = 0): Call<AirportResponse>


    @GET("v1/operations/schedules/{origin}/{destination}/{time}?directFlights=1")
    fun getFlights(@Path("origin") origin: String,
                   @Path("destination") destination: String,
                   @Path("time") time: String,
                   @Query("offset") offset: Int = 0): Call<ScheduleResponse>

    @POST("v1/oauth/token")
    fun requestToken(@Body request: RequestBody): Call<TokenResponse?>


    companion object Factory {

        val services = createServices()

        private fun createServices(): LufthansaServices {


            val loggingInterceptor = HttpLoggingInterceptor()

            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val authenticator = TokenAuthenticator()

            val client = OkHttpClient()
                    .newBuilder().also {
                        if (authenticator.token != null) {
                            it.addInterceptor { chain ->
                                val request = chain.request()
                                        .newBuilder()
                                        .addHeader("Authorization", "Bearer ${authenticator.token}")
                                        .build()
                                return@addInterceptor chain.proceed(request)
                            }
                        }
                    }

                    .addInterceptor(loggingInterceptor)
                    .authenticator(authenticator)
                    .build()




            return Retrofit.Builder()
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BuildConfig.endPoint)
                    .build()
                    .create(LufthansaServices::class.java)
        }

        class TokenAuthenticator : Authenticator {


            var token: String? = null

            @Throws(IOException::class)
            override fun authenticate(route: Route?, response: Response): Request? {
                if (response.request().header("Authorization") != null) {
                    return null
                }


                val formBody = FormBody.Builder()
                        .add("client_id", BuildConfig.lhAppKey)
                        .add("client_secret", BuildConfig.lhAppSecret)
                        .add("grant_type", "client_credentials")
                        .build()

                token = services.requestToken(formBody).execute().body()?.accessToken

                return response.request().newBuilder()
                        .header("Authorization", "Bearer $token")
                        .build()
            }
        }


    }
}



