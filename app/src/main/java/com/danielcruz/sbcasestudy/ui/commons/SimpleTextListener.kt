package com.danielcruz.sbcasestudy.ui.commons

import android.text.Editable
import android.text.TextWatcher

class SimpleTextListener(private val callback: (String) -> Unit) : TextWatcher {
    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        callback.invoke(s.toString())

    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
    }

}