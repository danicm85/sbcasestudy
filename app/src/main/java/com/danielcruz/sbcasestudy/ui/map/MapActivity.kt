package com.danielcruz.sbcasestudy.ui.map

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.util.Log
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.data.FlightDto
import com.danielcruz.sbcasestudy.data.JourneyData
import com.danielcruz.sbcasestudy.ui.base.BaseActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.item_flight.*
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*


class MapActivity : BaseActivity() {

    data class Journey(val origin: String, val destination: String) : Serializable

    companion object {

        val HOUR_FORMATTER = SimpleDateFormat("HH:mm", Locale.US)
        const val ARG_FLIGHT = "journey"

        fun show(caller: Activity, flight: FlightDto) {

            Intent(caller, MapActivity::class.java)
                    .putExtra(ARG_FLIGHT, flight)
                    .also { caller.startActivity(it) }
        }
    }


    override fun getLayout() = R.layout.activity_map

    private lateinit var googleMap: GoogleMap

    private lateinit var vm: MapViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setTitle(R.string.title_details)
        vm = ViewModelProviders.of(this).get(MapViewModel::class.java)


        (supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.apply {
            getMapAsync { it ->
                googleMap = it
                val flight = intent.getSerializableExtra(ARG_FLIGHT) as FlightDto
                vm.getJourney(flight).observe(this, Observer { data ->

                    when (data) {
                        is JourneyData.FullLoad -> {
                            showDetail(data.journey.flight)
                            drawOnMap(data.journey.origin, data.journey.destination)
                        }
                        is JourneyData.LoadError -> Log.d("TEST", "load error")
                    }

                })
            }
        }
    }

    private fun showDetail(item: FlightDto) {
        departureAirport.text = item.departureAirport
        departureTime.text = HOUR_FORMATTER.format(item.localDepartureTime)
        if (item.arrivalTerminal.isNotEmpty()) {
            arrivalTerminalAirport.text = "${item.arrivalAirport} (T${item.arrivalTerminal})"
        } else {
            arrivalTerminalAirport.text = item.arrivalAirport
        }

        arrivalTime.text = HOUR_FORMATTER.format(item.localArrivalTime)
        flightNumber.text = "${item.airline} ${item.flightNumber}"
    }

    private fun drawOnMap(origin: Airport, destination: Airport) {
        googleMap.apply {

            if (origin.latitude == null || origin.longitude == null || destination.longitude == null || destination.latitude == null) {
                return
            }

            val originLatLng = LatLng(origin.latitude.toDouble(), origin.longitude.toDouble())
            val destinationLatLng = LatLng(destination.latitude.toDouble(), destination.longitude.toDouble())

            addPolyline(
                    PolylineOptions()
                            .add(originLatLng, destinationLatLng)
                            .color(ContextCompat.getColor(this@MapActivity, R.color.colorAccent))
                            .geodesic(true)
            )

            addMarker(MarkerOptions().position(originLatLng).title(origin.toString()))
            addMarker(MarkerOptions().position(destinationLatLng).title(destination.toString()))
            moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLng, 3f))

            departureAirport.setOnClickListener { animateCamera(CameraUpdateFactory.newLatLngZoom(originLatLng, 4f)) }
            arrivalTerminalAirport.setOnClickListener { animateCamera(CameraUpdateFactory.newLatLngZoom(destinationLatLng, 4f)) }


        }

    }


}
