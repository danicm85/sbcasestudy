package com.danielcruz.sbcasestudy.ui.airports

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.ui.base.BaseActivity
import com.danielcruz.sbcasestudy.ui.commons.SimpleTextListener
import kotlinx.android.synthetic.main.content_airport_selection.*
import java.io.Serializable

class AirportSelectionActivity : BaseActivity() {

    companion object {

        const val RESULT_KEY_AIRPORT = "result_airport"
        const val RESULT_KEY_MODE = "result_mode"

        fun start(caller: Activity, requestCode: Int, mode: Mode) {
            val intent = Intent(caller, AirportSelectionActivity::class.java)
                    .putExtra("mode", mode)

            caller.startActivityForResult(intent, requestCode)
        }
    }


    private lateinit var adapter: AirportAdapter
    private lateinit var viewModel: AirportViewModel
    private lateinit var currentMode: Mode


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupView(intent.extras)

        viewModel = ViewModelProviders.of(this).get(AirportViewModel::class.java)

        val obs = Observer<List<Airport>> { it ->
            progressBar.visibility = View.INVISIBLE
            airportList.visibility = View.VISIBLE
            adapter.data = it ?: emptyList()
        }

        airportFilter.addTextChangedListener(SimpleTextListener { text ->

            progressBar.visibility = View.VISIBLE
            viewModel.searchAirports(text.trim()).observe(this, obs)

        })

        airportFilter.setText("")

        // viewModel.searchAirports("").observe(this, obs)

    }

    override fun getLayout() = R.layout.activity_airport_selection

    private fun setupView(extras: Bundle?) {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        extras?.getSerializable("mode")?.let {
            val title = when (it as Mode) {
                is Origin -> R.string.title_origin
                is Destination -> R.string.title_destination
            }
            supportActionBar?.title = getString(title)
            this.currentMode = it
        }

        airportList.layoutManager = LinearLayoutManager(this)
        adapter = AirportAdapter { finishWithResult(it) }
        airportList.adapter = adapter
        airportList.visibility = View.INVISIBLE

    }

    private fun finishWithResult(airport: Airport) {

        Intent().apply {
            putExtra(RESULT_KEY_AIRPORT, airport)
            putExtra(RESULT_KEY_MODE, currentMode)

        }.let {
            setResult(Activity.RESULT_OK, it)
            finish()
        }


    }
}

sealed class Mode : Serializable
object Origin : Mode()
object Destination : Mode()