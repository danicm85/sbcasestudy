package com.danielcruz.sbcasestudy.ui.flight

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.ui.airports.AirportSelectionActivity
import com.danielcruz.sbcasestudy.ui.airports.Destination
import com.danielcruz.sbcasestudy.ui.airports.Mode
import com.danielcruz.sbcasestudy.ui.airports.Origin
import com.danielcruz.sbcasestudy.ui.base.BaseActivity
import com.danielcruz.sbcasestudy.ui.selection.FlightSelectionActivity
import kotlinx.android.synthetic.main.content_flight_search.*

class FlightSearchActivity : BaseActivity() {

    companion object {
        const val REQUEST_CODE_AIRPORT = 0
    }

    private lateinit var vm: FlightSearchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        vm = ViewModelProviders.of(this).get(FlightSearchViewModel::class.java)

        setupView()

        subscribe()


    }

    override fun getLayout() = R.layout.activity_flight_search

    private fun subscribe() {
        vm.action.observe(this, Observer {

            it?.getContentNotHandled()?.let { event ->
                when (event) {
                    is FlightSearchViewModel.Action.ClearInput -> clearInput()
                    is FlightSearchViewModel.Action.ShowDetail -> showDetail(event)
                }
            }


        })

        vm.message.observe(this, Observer {

            Log.d("Test", it.toString())

            val messageRes =
                    when (it) {
                        is FlightSearchViewModel.Message.DuplicateAirport -> R.string.duplicate_airport
                        is FlightSearchViewModel.Message.MissingOrigin -> R.string.missing_origin
                        is FlightSearchViewModel.Message.MissingDestination -> R.string.missing_destination
                        else -> 0
                    }

            if (messageRes == 0) {
                clearMessage()
            } else {
                showMessage(messageRes)
            }

        })

        vm.state.observe(this, Observer {
            it?.let { state ->
                setAirport(selectedDestination, state.destination, R.string.title_destination)
                setAirport(selectedOrigin, state.origin, R.string.title_origin)
                calendarView.date = state.date.time
            }
        })
    }

    private fun clearInput() {


    }

    private fun showDetail(detail: FlightSearchViewModel.Action.ShowDetail) {

        FlightSelectionActivity.open(this, FlightSelectionActivity.Companion
                .QueryArguments(detail.origin, detail.destination, detail.date))
    }

    private fun setAirport(target: TextView, airport: Airport?, fallback: Int) {
        target.text = airport?.toString() ?: getString(fallback)
    }

    private fun setupView() {

        View.OnClickListener {
            AirportSelectionActivity.start(this, REQUEST_CODE_AIRPORT, Destination)
        }.let {
            destinationBtn.setOnClickListener(it)
            selectedDestination.setOnClickListener(it)
        }

        View.OnClickListener {
            AirportSelectionActivity.start(this, REQUEST_CODE_AIRPORT, Origin)
        }.let {
            originBtn.setOnClickListener(it)
            selectedOrigin.setOnClickListener(it)
        }

        calendarView.setOnDateChangeListener { _, year, month, dayOfMonth -> vm.dateChanged(year, month, dayOfMonth) }

        acceptFlight.setOnClickListener { vm.queryRequested() }

        supportActionBar?.setTitle(R.string.title_flight_search)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            REQUEST_CODE_AIRPORT -> processAirportSelection(resultCode, data)
            else -> super.onActivityResult(requestCode, resultCode, data)
        }


    }

    private fun processAirportSelection(resultCode: Int, data: Intent?) {

        if (resultCode == Activity.RESULT_OK) {

            data?.extras?.let {

                val airport = it.getSerializable(AirportSelectionActivity.RESULT_KEY_AIRPORT) as Airport?
                val mode = it.getSerializable(AirportSelectionActivity.RESULT_KEY_MODE) as Mode

                when (mode) {
                    is Origin -> vm.originSelected(airport)
                    is Destination -> vm.destinationSelected(airport)
                }


            }
        }

    }
}
