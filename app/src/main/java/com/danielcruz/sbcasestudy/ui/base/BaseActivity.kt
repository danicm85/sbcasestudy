package com.danielcruz.sbcasestudy.ui.base

import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import com.danielcruz.sbcasestudy.R

abstract class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayout())
        findViewById<Toolbar>(R.id.toolbar)?.let {
            setSupportActionBar(it)
        }
    }

    protected abstract fun getLayout(): Int

    private var snackBar: Snackbar? = null
    protected fun showMessage(message: Int) {

        clearMessage()
        findViewById<CoordinatorLayout>(R.id.coordinator)?.let {
            snackBar = Snackbar.make(it, message, Snackbar.LENGTH_LONG).also { created ->
                created.show()
            }
        }
    }

    protected fun clearMessage() {
        snackBar?.dismiss()
    }

}