package com.danielcruz.sbcasestudy.ui.selection

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.FlightDto
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_flight.*
import java.text.SimpleDateFormat
import java.util.*

class FlightAdapter(private val selectionCallback: (FlightDto) -> (Unit)) : RecyclerView.Adapter<AirportViewHolder>() {


    var data: List<FlightDto> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): AirportViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_flight, parent, false)
        return AirportViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: AirportViewHolder, position: Int) {

        holder.itemView.setOnClickListener { selectionCallback.invoke(data[holder.adapterPosition]) }
        holder.bind(data[position])
    }

}


// https://proandroiddev.com/kotlin-android-extensions-using-view-binding-the-right-way-707cd0c9e648

class AirportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView


    fun bind(item: FlightDto) {
        departureAirport.text = item.departureAirport
        departureTime.text = HOUR_FORMATTER.format(item.localDepartureTime)
        if (item.arrivalTerminal.isNotEmpty()) {
            arrivalTerminalAirport.text = "${item.arrivalAirport} (T${item.arrivalTerminal})"
        } else {
            arrivalTerminalAirport.text = item.arrivalAirport
        }

        arrivalTime.text = HOUR_FORMATTER.format(item.localArrivalTime)
        flightNumber.text = "${item.airline} ${item.flightNumber}"

    }

    companion object {
        val HOUR_FORMATTER = SimpleDateFormat("HH:mm", Locale.US)
    }

}

