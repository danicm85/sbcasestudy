package com.danielcruz.sbcasestudy.ui.airports

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.Airport
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.airport_item.*

class AirportAdapter(private val selectionCallback: (Airport) -> (Unit)) : RecyclerView.Adapter<AirportViewHolder>() {


    var data: List<Airport> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, type: Int): AirportViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.airport_item, parent, false)
        return AirportViewHolder(view)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: AirportViewHolder, position: Int) {

        holder.itemView.setOnClickListener { selectionCallback.invoke(data[holder.adapterPosition]) }
        holder.bind(data[position])
    }

}


// https://proandroiddev.com/kotlin-android-extensions-using-view-binding-the-right-way-707cd0c9e648

class AirportViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    fun bind(item: Airport) {
        airportName.text = item.name
        airportCode.text = item.airportCode
    }

}

