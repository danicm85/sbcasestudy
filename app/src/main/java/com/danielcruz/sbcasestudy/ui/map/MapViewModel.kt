package com.danielcruz.sbcasestudy.ui.map

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.danielcruz.sbcasestudy.data.FlightDto
import com.danielcruz.sbcasestudy.data.JourneyData
import com.danielcruz.sbcasestudy.data.Repository

class MapViewModel(application: Application) : AndroidViewModel(application) {


    private val repository = Repository(application)


    fun getJourney(flight: FlightDto): LiveData<JourneyData> {
        return repository.findJourney(flight)
    }
}