package com.danielcruz.sbcasestudy.ui.flight

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import com.danielcruz.sbcasestudy.data.Airport
import java.util.*

class FlightSearchViewModel(context: Application) : AndroidViewModel(context) {


    var state: MutableLiveData<ViewState> = MutableLiveData()
    var message: MutableLiveData<Message> = MutableLiveData()
    var action: MutableLiveData<Event> = MutableLiveData()

    init {
        state.value = ViewState(null, null, Calendar.getInstance().time)
        message.value = Message.Empty
        action.value = Event(Action.None)
    }

    fun originSelected(airport: Airport?) {

        message.value = Message.Empty
        state.value = state.value!!.copy(origin = airport)

    }


    fun destinationSelected(airport: Airport?) {
        message.value = Message.Empty
        state.value = state.value!!.copy(destination = airport)
    }

    fun dateChanged(year: Int, month: Int, dayOfMonth: Int) {
        message.value = Message.Empty

        Calendar.getInstance().apply {
            clear()
            set(year, month, dayOfMonth)
        }.also {
            state.value = state.value!!.copy(date = it.time)
        }

    }

    fun queryRequested() {

        state.value?.let {
            when {
                it.destination == null -> message.value = Message.MissingDestination
                it.origin == null -> message.value = Message.MissingOrigin
                it.origin == it.destination -> message.value = Message.DuplicateAirport
                else -> {
                    message.value = Message.Empty
                    action.value = Event(Action.ShowDetail(it.origin, it.destination, it.date))
                }
            }
        }

    }


    data class ViewState(val origin: Airport?, val destination: Airport?, val date: Date)


    sealed class Message {

        object MissingOrigin : Message()
        object MissingDestination : Message()
        object DuplicateAirport : Message()
        object Empty : Message()
    }

    sealed class Action {
        data class ShowDetail(val origin: Airport, val destination: Airport, val date: Date) : Action()
        object ClearInput : Action()
        object None : Action()
    }

    //https://medium.com/androiddevelopers/livedata-with-snackbar-navigation-and-other-events-the-singleliveevent-case-ac2622673150
    open class Event(private val content: Action) {
        var handled = false
            private set

        fun getContentNotHandled() =
                if (handled) {
                    null
                } else {
                    handled = true
                    content
                }
    }
}