package com.danielcruz.sbcasestudy.ui.selection

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.data.FlightDto
import com.danielcruz.sbcasestudy.data.Repository
import java.util.*

class FlightSelectionViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = Repository(application)

    private var flights: LiveData<List<FlightDto>>? = null


    private lateinit var mOrigin: Airport
    private lateinit var mDestination: Airport
    private lateinit var mDate: Date


    fun query(origin: Airport, destination: Airport, date: Date): LiveData<List<FlightDto>> {

        mOrigin = origin
        mDestination = destination
        mDate = date

        var mFlights = flights

        return if (mFlights == null) {
            mFlights = repository.getFlights(origin.airportCode, destination.airportCode, date)
            flights = mFlights
            mFlights
        } else {
            mFlights
        }
    }

}