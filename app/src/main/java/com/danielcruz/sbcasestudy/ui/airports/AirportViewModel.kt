package com.danielcruz.sbcasestudy.ui.airports

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.data.Repository

class AirportViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = Repository(application)
    val airports = repository.airports


    fun searchAirports(part: String): LiveData<List<Airport>> {
        return repository.findAirports(part)
    }

}