package com.danielcruz.sbcasestudy.ui.selection

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.danielcruz.sbcasestudy.R
import com.danielcruz.sbcasestudy.data.Airport
import com.danielcruz.sbcasestudy.data.FlightDto
import com.danielcruz.sbcasestudy.ui.base.BaseActivity
import com.danielcruz.sbcasestudy.ui.map.MapActivity
import kotlinx.android.synthetic.main.content_flight_selection.*
import java.io.Serializable
import java.util.*


class FlightSelectionActivity : BaseActivity() {


    companion object {
        data class QueryArguments(val origin: Airport, val destination: Airport, val date: Date) : Serializable

        private const val QUERY_ARG = "query"

        fun open(caller: Activity, arguments: QueryArguments) {

            Intent(caller, FlightSelectionActivity::class.java).putExtra(QUERY_ARG, arguments)
                    .also { caller.startActivity(it) }
        }

    }

    private lateinit var vm: FlightSelectionViewModel
    private lateinit var adapter: FlightAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val (origin, destination, date) = intent.getSerializableExtra(QUERY_ARG) as QueryArguments
        setupView()

        supportActionBar?.title = "${origin.name} - ${destination.name}"


        vm = ViewModelProviders.of(this).get(FlightSelectionViewModel::class.java)
        vm.query(origin, destination, date).observe(this, Observer {
            when {
                it == null -> {
                    showErrorWarn()
                }
                it.isEmpty() -> {
                    showNoResultsWarn()
                }
                else -> {
                    hideWarn()
                    adapter.data = it
                }
            }
        })
    }

    private fun showNoResultsWarn() {

        textWarn.setText(R.string.warn_no_results)
        iconWarn.setImageResource(R.drawable.ic_mood_bad_black_24dp)

        textWarn.visibility = View.VISIBLE
        iconWarn.visibility = View.VISIBLE

    }

    private fun showErrorWarn() {


        textWarn.setText(R.string.warn_error)
        iconWarn.setImageResource(R.drawable.ic_error_outline_black_24dp)

        textWarn.visibility = View.VISIBLE
        iconWarn.visibility = View.VISIBLE

    }

    private fun hideWarn() {

        textWarn.visibility = View.INVISIBLE
        iconWarn.visibility = View.INVISIBLE

    }

    override fun getLayout() = R.layout.activity_flight_selection

    private fun setupView() {

        hideWarn()

        flightList.layoutManager = LinearLayoutManager(this).also {
            flightList.addItemDecoration(DividerItemDecoration(this, it.orientation))
        }



        adapter = FlightAdapter {
            flightSelected(it)
        }

        flightList.adapter = adapter

    }

    private fun flightSelected(flight: FlightDto) {

        MapActivity.show(this, flight)

    }
}
