package com.danielcruz.sbcasestudy.data

import android.app.Application
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.danielcruz.sbcasestudy.BuildConfig
import com.danielcruz.sbcasestudy.data.persistance.AirportDao
import com.danielcruz.sbcasestudy.data.persistance.AppDataBase
import com.danielcruz.sbcasestudy.network.LufthansaServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class Repository(application: Application) {

    companion object {
        const val AIRPORT_LAST_UPDATE_PREF_KEY = "airport_last_download"
        const val UPDATE_THRESHOLD_MILLIS = 24 * 60 * 60 * 1000
        val DATE_REQUEST_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.US)
    }

    private val airportDao = AppDataBase.getInstance(application).airportDao()
    private val services = LufthansaServices.services
    private val preferences = application.getSharedPreferences("repository", Context.MODE_PRIVATE)

    var airports = airportDao.getAll()

    init {
        checkAirports()
    }


    fun getFlights(origin: String, destination: String, date: Date): LiveData<List<FlightDto>> {

        val result = MutableLiveData<List<FlightDto>>()
        val time = DATE_REQUEST_FORMAT.format(date)

        services.getFlights(origin, destination, time).enqueue(object : Callback<ScheduleResponse> {

            override fun onFailure(call: Call<ScheduleResponse>, t: Throwable) {

                result.value = null

                if (BuildConfig.DEBUG) {
                    Log.e("Repository", "error requesting $origin -> $destination, $time", t)
                }
            }

            override fun onResponse(call: Call<ScheduleResponse>, response: Response<ScheduleResponse>) {

                result.value = when {
                    response.code() == 200 -> FlightMapper.mapFromWs(response.body())
                    response.code() == 404 -> emptyList()
                    else -> null
                }

            }
        })

        return result

    }

    //https://medium.com/androiddevelopers/livedata-beyond-the-viewmodel-reactive-patterns-using-transformations-and-mediatorlivedata-fda520ba00b7
    fun findJourney(flight: FlightDto): LiveData<JourneyData> {

        val foundOrigin = airportDao.findByAirportCode(flight.departureAirport)
        val foundDest = airportDao.findByAirportCode(flight.arrivalAirport)

        val result = MediatorLiveData<JourneyData>()

        result.addSource(foundOrigin) { result.value = combine(flight, foundOrigin, foundDest) }
        result.addSource(foundDest) { result.value = combine(flight, foundOrigin, foundDest) }

        return result

    }

    private fun combine(flight: FlightDto, foundOrigin: LiveData<Airport?>, foundDest: LiveData<Airport?>): JourneyData {

        val origin = foundOrigin.value
        val destination = foundDest.value

        if (origin == null || destination == null) return JourneyData.PartialLoad

        return JourneyData.FullLoad(Journey(flight.copy(), origin, destination))

    }


    private fun checkAirports() {

        val now = System.currentTimeMillis()

        downloadAirports(now, requiresAirportDownload(now))

    }

    private fun downloadAirports(now: Long, requiresAirportDownload: Boolean) {
        registerAirportDownload(now)
        DownloadTask(airportDao, services, requiresAirportDownload)
                .execute()
    }

    private fun registerAirportDownload(now: Long) {
        preferences.edit().putLong(AIRPORT_LAST_UPDATE_PREF_KEY, now).apply()
    }

    private fun requiresAirportDownload(now: Long): Boolean {
        val lastDownload = preferences.getLong(AIRPORT_LAST_UPDATE_PREF_KEY, -1)
        return lastDownload < 0 || now - lastDownload > UPDATE_THRESHOLD_MILLIS
    }

    fun findAirports(part: String): LiveData<List<Airport>> {
        return airportDao.findByAirportName(part)
    }

}

private class DownloadTask(val airportDao: AirportDao, val services: LufthansaServices, val expiredData: Boolean) : AsyncTask<Unit, Unit, Unit>() {


    override fun doInBackground(vararg params: Unit?) {


        if (expiredData || airportDao.findAny() == null) {


            var expected = 0
            var received = 0

            try {
                do {

                    val response = services.getAirports(received).execute()

                    if (response.isSuccessful) {

                        response.body()?.resource?.let {

                            expected = it.meta?.totalCount ?: 0
                            val data = it.airports?.airport
                            val lastReceived = data?.size ?: 0

                            if (data != null && lastReceived > 0) {
                                received += lastReceived
                                airportDao.insertAll(AirportMapper.map(data))
                            }

                        }

                    } else {
                        expected = 0
                    }

                } while (received < expected)

            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.e("DownloadTask", "doInBackground", e)
                }
            }
        }
    }

}