package com.danielcruz.sbcasestudy.data

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable
import java.util.*

@Entity
data class Airport(
        val name: String,
        @PrimaryKey
        val airportCode: String,
        val countryCode: String,
        val latitude: Float?,
        val longitude: Float?,
        val utcOffset: Float) : Serializable {
    override fun toString() = "$name ($airportCode)"
}


data class FlightDto(
        val departureAirport: String,
        val arrivalAirport: String,
        val arrivalTerminal: String,
        val localDepartureTime: Date?,
        val localArrivalTime: Date?,
        val flightNumber: String,
        val airline: String

) : Serializable

data class Journey(val flight: FlightDto, val origin: Airport, val destination: Airport)

sealed class JourneyData {
    object PartialLoad : JourneyData()
    object LoadError : JourneyData()
    data class FullLoad(val journey: Journey) : JourneyData()
}