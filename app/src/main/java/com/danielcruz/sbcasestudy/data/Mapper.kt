package com.danielcruz.sbcasestudy.data

import android.util.Log
import com.danielcruz.sbcasestudy.BuildConfig
import java.text.SimpleDateFormat
import java.util.*

object AirportMapper {

    fun mapFromWs(source: AirportResponse?): List<Airport> {


        source?.resource?.airports?.airport?.let {
            return map(it)
        }

        return mutableListOf()

    }

    fun map(source: List<AirportItem>): List<Airport> =
            source.map {
                Airport(
                        it.names.name?.value ?: "",
                        it.airportCode,
                        it.countryCode,
                        it.position.coordinate?.latitude,
                        it.position.coordinate?.longitude,
                        it.utcOffset)
            }


}

object FlightMapper {

    private const val LOCAL_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm"

    fun mapFromWs(source: ScheduleResponse?): List<FlightDto> {

        return source?.scheduleResource?.schedule?.map {

            FlightDto(
                    it.flight.departure.airportCode,
                    it.flight.arrival.airportCode,
                    it.flight.arrival.terminal?.name?.toString() ?: "",
                    it.flight.departure.scheduledTimeLocal.dateTime.asDate(LOCAL_TIME_FORMAT),
                    it.flight.arrival.scheduledTimeLocal.dateTime.asDate(LOCAL_TIME_FORMAT),
                    it.flight.marketingCarrier.flightNumber, it.flight.marketingCarrier.airlineID)

        } ?: mutableListOf()

    }


    private fun String.asDate(pattern: String): Date? =

            try {
                SimpleDateFormat(pattern, Locale.UK).parse(this)
            } catch (e: Exception) {
                if (BuildConfig.DEBUG) {
                    Log.e("String.asDate", pattern + " " + this, e)
                }
                null
            }

}