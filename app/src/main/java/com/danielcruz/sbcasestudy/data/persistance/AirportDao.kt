package com.danielcruz.sbcasestudy.data.persistance

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.danielcruz.sbcasestudy.data.Airport

@Dao
interface AirportDao {

    @Query("SELECT * FROM Airport ORDER BY name ASC")
    fun getAll(): LiveData<List<Airport>>

    @Query("SELECT * FROM Airport WHERE airportCode = :code")
    fun findByAirportCode(code: String): LiveData<Airport?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(airports: List<Airport>)

    @Query("DELETE FROM Airport")
    fun deleteAll()

    @Query("SELECT * FROM Airport WHERE name LIKE '%' || :part || '%'")
    fun findByAirportName(part: String): LiveData<List<Airport>>

    @Query("SELECT * FROM Airport LIMIT 1")
    fun findAny(): Airport?


}