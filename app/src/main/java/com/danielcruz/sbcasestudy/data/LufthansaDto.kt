package com.danielcruz.sbcasestudy.data


import com.google.gson.annotations.SerializedName


// AIRPORTS

data class AirportResponse(@SerializedName("AirportResource")
                           val resource: AirportResource)

data class Names(@SerializedName("Name")
                 val name: NameItem?)


data class AirportItem(@SerializedName("CityCode")
                       val cityCode: String = "",
                       @SerializedName("Names")
                       val names: Names,
                       @SerializedName("UtcOffset")
                       val utcOffset: Float = 0.0f,
                       @SerializedName("AirportCode")
                       val airportCode: String = "",
                       @SerializedName("Position")
                       val position: Position,
                       @SerializedName("TimeZoneId")
                       val timeZoneId: String = "",
                       @SerializedName("CountryCode")
                       val countryCode: String = "",
                       @SerializedName("LocationType")
                       val locationType: String = "")


data class NameItem(@SerializedName("@LanguageCode")
                    val languageCode: String = "",
                    @SerializedName("$")
                    val value: String = "")


data class Coordinate(@SerializedName("Latitude")
                      val latitude: Float = 0.0f,
                      @SerializedName("Longitude")
                      val longitude: Float = 0.0f)


data class Position(@SerializedName("Coordinate")
                    val coordinate: Coordinate?)


data class Airports(@SerializedName("Airport")
                    val airport: List<AirportItem>?)


data class AirportResource(@SerializedName("Meta")
                           val meta: Meta?,
                           @SerializedName("Airports")
                           val airports: Airports?)


// SCHEDULES

data class ScheduleResponse(@SerializedName("ScheduleResource")
                            val scheduleResource: ScheduleResource)

data class ScheduleItem(@SerializedName("Flight")
                        val flight: FlightItem,
                        @SerializedName("TotalJourney")
                        val totalJourney: TotalJourney)


data class FlightItem(@SerializedName("Details")
                      val details: Details,
                      @SerializedName("Equipment")
                      val equipment: Equipment,
                      @SerializedName("Departure")
                      val departure: Departure,
                      @SerializedName("MarketingCarrier")
                      val marketingCarrier: MarketingCarrier,
                      @SerializedName("OperatingCarrier")
                      val operatingCarrier: OperatingCarrier,
                      @SerializedName("Arrival")
                      val arrival: Arrival)


data class Arrival(@SerializedName("AirportCode")
                   val airportCode: String = "",
                   @SerializedName("Terminal")
                   val terminal: Terminal?,
                   @SerializedName("ScheduledTimeLocal")
                   val scheduledTimeLocal: ScheduledTimeLocal)


data class Terminal(@SerializedName("Name")
                    val name: String = "")


data class Details(@SerializedName("Stops")
                   val stops: Stops,
                   @SerializedName("DaysOfOperation")
                   val daysOfOperation: Int = 0,
                   @SerializedName("DatePeriod")
                   val datePeriod: DatePeriod)


data class Equipment(@SerializedName("AircraftCode")
                     val aircraftCode: String = "")


data class ScheduledTimeLocal(@SerializedName("DateTime")
                              val dateTime: String = "")


data class Departure(@SerializedName("AirportCode")
                     val airportCode: String = "",
                     @SerializedName("ScheduledTimeLocal")
                     val scheduledTimeLocal: ScheduledTimeLocal)


data class MarketingCarrier(@SerializedName("AirlineID")
                            val airlineID: String = "",
                            @SerializedName("FlightNumber")
                            val flightNumber: String = "")


data class OperatingCarrier(@SerializedName("AirlineID")
                            val airlineID: String = "")


data class Stops(@SerializedName("StopQuantity")
                 val stopQuantity: Int = 0)


data class DatePeriod(@SerializedName("Expiration")
                      val expiration: String = "",
                      @SerializedName("Effective")
                      val effective: String = "")


data class TotalJourney(@SerializedName("Duration")
                        val duration: String = "")


data class ScheduleResource(@SerializedName("Meta")
                            val meta: Meta,
                            @SerializedName("Schedule")
                            val schedule: List<ScheduleItem>?)


// COMMON

data class Meta(@SerializedName("@Version")
                val version: String = "",
                @SerializedName("TotalCount")
                val totalCount: Int = 0,
                @SerializedName("Link")
                val link: List<LinkItem>?)

data class LinkItem(@SerializedName("@Href")
                    val href: String = "",
                    @SerializedName("@Rel")
                    val rel: String = "")


data class TokenResponse(@SerializedName("access_token")
                         val accessToken: String = "",
                         @SerializedName("token_type")
                         val tokenType: String = "",
                         @SerializedName("expires_in")
                         val expiresIn: Int = 0)
