package com.danielcruz.sbcasestudy.data.persistance

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.danielcruz.sbcasestudy.data.Airport

@Database(entities = [Airport::class], version = 1)
abstract class AppDataBase : RoomDatabase() {

    abstract fun airportDao(): AirportDao

    companion object : SingletonHolder<AppDataBase, Context>({
        Room.databaseBuilder(it, AppDataBase::class.java, "main.db").build()
    })
}


//https://medium.com/@BladeCoder/kotlin-singletons-with-argument-194ef06edd9e
open class SingletonHolder<out T, in A>(creator: (A) -> T) {
    private var creator: ((A) -> T)? = creator
    @Volatile
    private var instance: T? = null

    fun getInstance(arg: A): T {
        val i = instance
        if (i != null) {
            return i
        }

        return synchronized(this) {
            val i2 = instance
            if (i2 != null) {
                i2
            } else {
                val created = creator!!(arg)
                instance = created
                creator = null
                created
            }
        }
    }
}

